import random
import sys


def main():
    print_header()
    word = choose_word()
    split_word, blanks = create_blank_board(word)
    game_loop(blanks, split_word, word)


def game_loop(blank_word, split_word, word):
    lives = 3
    correct_guesses = 0
    guess = None
    win = 1

    print("You have been sentenced to death...")
    print("...although if you can guess the word the court is thinking of, you will be spared...\n")

    while guess != "quit":
        print("Your word is:")
        print(" ".join(blank_word).upper())

        guess = input("\nWhich letter will you guess? >> ")
        guess = guess.lower().strip()

        if len(guess) == 1:
            correct_guesses, lives = check_for_letter(guess, split_word, blank_word, lives, correct_guesses)
            win = win_condition(blank_word, correct_guesses, word)
        elif len(guess) > 1 and guess != "quit":
            print("Please only choose one letter at a time.\n")

        if lives == 0:
            print("...time to be dead...")
            print("...YOU'VE BEEN HUNG. GOOD BYE.")
            sys.exit(0)
        elif win == 0:
            sys.exit(0)

    print("By not playing, you leave us no choice but to hang you anyway. Good bye forever.")
    sys.exit(0)


def print_header():
    print(30 * "–")
    print("         H A N G M A N")
    print(30 * "–")


def choose_word():
    word_list = [
        "arse",
        "bum",
        "penis",
        "beyn",
        "loyf",
    ]

    word = random.choice(word_list)
    return word


def create_blank_board(word):
    hidden_word = []
    active_word = list(word)

    for _ in active_word:
        hidden_word.append("_")

    return active_word, hidden_word


def check_for_letter(guess, split_word, blank_word, lives, correct_guesses):
    if guess in split_word:
        print(f"\nYes! \"{guess.upper()}\" is in there!\n")

        found = split_word.index(guess)
        blank_word[found] = guess

        correct_guesses += 1
    elif guess == "quit":
        return
    else:
        lives -= 1

        print(f"\nNo, \"{guess}\" is not there...")

        if lives != 0:
            print(f"...you have {lives} steps to the noose...\n")

    return correct_guesses, lives


def win_condition(active_word, guesses, word):
    if guesses == len(active_word):
        print(f"You have survived the hangman. The word was \"{word}\". Run free!")

        win = 0
        return win
    else:
        return


if __name__ == "__main__":
    main()
