import random


def main():
    print_header()
    word = choose_word()
    blanks = create_blank_board(word)
    game_loop(blanks, word)


def game_loop(blank_word, str_word):
    lives = 3
    correct_guesses = 0
    win = 1

    running = True

    print("You have been sentenced to death...")
    print("...although if you can guess the word I'm thinking of, you will be spared...\n")

    while running:
        display_word(blank_word)
        guess = input("\nWhich letter will you guess? >> ")
        guess = guess.lower().strip()

        if len(guess) > 1 and guess != "quit":
            print("Please only choose one letter at a time.\n")
        elif guess == "quit":
            running = False
        else:
            correct_guesses, lives = check_for_letter(guess, blank_word, lives, correct_guesses)
            win = win_condition(blank_word, correct_guesses, str_word)

        if lives == 0:
            print("You've been hung!")
            running = False
        elif win == 0:
            running = False

    print("Thanks for playing!")


def print_header():
    print(30 * "–")
    print("         H A N G M A N")
    print(30 * "–")


def choose_word():
    word_list = [
        "arse",
        "bum",
        "penis",
        "beyn",
        "loyf",
    ]

    word = random.choice(word_list)
    return word


def create_blank_board(word):  # TODO: Allow for recurring letters in a dict
    active_word = {}

    for letter in word:
        active_word.update({letter: False},)

    return active_word


def display_word(active_word):
    print(f"Here is your word:", end=" ")

    for k, v in active_word.items():
        if v is False:
            print("_", end=" ")
        else:
            print(k.upper(), end=" ")

    return active_word


def check_for_letter(guess, blanks, lives, correct_guesses):
    if guess in blanks:
        print(f"\nYes! \"{guess.upper()}\" is in there!\n")
        blanks[guess] = True
        correct_guesses += 1
    elif guess == "quit":
        return
    else:
        lives -= 1
        print(f"\nNo, \"{guess}\" is not there...")
        if lives != 0:
            print(f"...you have {lives} steps to the noose...\n")

    return correct_guesses, lives


def win_condition(active_word, guesses, word):
    if guesses == len(active_word):
        print(f"You have survived the hangman. The word was \"{word}\". Run free!")
        win = 0
        return win
    else:
        return


if __name__ == "__main__":
    main()
